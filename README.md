Setup:
Install Python - Install Python (version 3.10).

Set and activate virtual environment in project folder, execute the following commands:
pip install pipenv
export PIPENV_VENV_IN_PROJECT="enabled"
mkdir .venv
pipenv shell
source .venv/Scripts/activate

Install dependencies on virtual env in project folder, execute the following command:
pipenv install --dev

Enviroment variables:
In project folder, create a .env file from .env.example

Check your database configurations before you proceed

Run:
python main.py
