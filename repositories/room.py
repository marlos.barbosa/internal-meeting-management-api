from sqlalchemy.orm import Session
from domain.models.room import Room
from uuid import UUID

def get_rooms(db: Session):
    return db.query(Room).all()

def get_room_by_id(db: Session, room_id: UUID):
    return db.query(Room).filter(Room.id == room_id).first()

def get_room_by_name(db: Session, room_name: str):
    return db.query(Room).filter(Room.name == room_name).first()

def create_room(db: Session, room:Room):
    db.add(room)
    db.commit()
    db.refresh(room)
    return room

def delete_room_by_id(db: Session, room_id: UUID):
    room = get_room_by_id(db, room_id)
    db.delete(room)
    db.commit()
    return "Room deleted"

def update_room(db: Session, room_id: UUID, new_name: str):
    room = get_room_by_id(db, room_id)
    room.name = new_name
    db.commit()
    db.refresh(room)
    return room
