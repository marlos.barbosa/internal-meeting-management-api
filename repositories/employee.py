from sqlalchemy.orm import Session
from domain.models.employee import Employee
from uuid import UUID

def get_employees(db: Session):
    return db.query(Employee).all()

def get_employee_by_id(db: Session, employee_id: UUID):
    return db.query(Employee).filter(Employee.id == employee_id).first()

def get_employee_by_email(db: Session, email: str):
    return db.query(Employee).filter(Employee.email == email).first()

def create_employee(db: Session, employee:Employee):
    db.add(employee)
    db.commit()
    db.refresh(employee)
    return employee

def delete_employee_by_id(db: Session, employee_id: UUID):
    employee = get_employee_by_id(db, employee_id)
    db.delete(employee)
    db.commit()
    return "Employee deleted"

def update_employee(db: Session, employee_id: UUID, new_employee_data:Employee):
    employee = get_employee_by_id(db, employee_id)
    employee.name = new_employee_data.name
    employee.email = new_employee_data.email
    db.commit()
    db.refresh(employee)
    return employee
