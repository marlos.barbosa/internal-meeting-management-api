from sqlalchemy.orm import Session
from domain.models.meeting_employee import MeetingEmployee
from repositories import meeting as meeting_repository
from uuid import UUID

def get_meeting_employee(db: Session, meeting_id: int, employee_id: UUID):
    return db.query(MeetingEmployee).filter(MeetingEmployee.meeting_id == meeting_id, MeetingEmployee.employee_id == employee_id).first()

def get_meeting_employee_by_employee_id(db: Session, employee_id: UUID):
    return db.query(MeetingEmployee).filter(MeetingEmployee.employee_id == employee_id).all()


def add_employee_to_meeting(db: Session, meeting_id: UUID, employee_id: UUID):
    meeting_employee = MeetingEmployee(meeting_id=meeting_id, employee_id=employee_id)
    db.add(meeting_employee)
    db.commit()
    db.refresh(meeting_employee)
    return "Employee added to meeting"

def get_employee_availability(db: Session, employee_id: UUID, meeting_id: UUID):
    employee_meetings_id = db.query(MeetingEmployee).filter(MeetingEmployee.employee_id == employee_id).all()
    new_meeting = meeting_repository.get_meeting_by_id(db, meeting_id)
    for meeting in employee_meetings_id:
        existing_meeting = meeting_repository.get_meeting_by_id(db, meeting.meeting_id)
        if (new_meeting.start < existing_meeting.end and new_meeting.end > existing_meeting.start):
            return True
    return False

def remove_employee_from_meeting(db: Session, meeting_id: UUID, employee_id: UUID):
    meeting_employee = get_meeting_employee(db, meeting_id, employee_id)
    db.delete(meeting_employee)
    db.commit()
    return "Employee removed from meeting"