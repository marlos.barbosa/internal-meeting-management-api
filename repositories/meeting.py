from sqlalchemy.orm import Session
from domain.models.meeting import Meeting
from fastapi import HTTPException
from uuid import UUID
from repositories import room as room_repository

def get_meetings(db: Session):
    return db.query(Meeting).all()

def get_meeting_by_id(db: Session, meeting_id: UUID):
    return db.query(Meeting).filter(Meeting.id == meeting_id).first()

def get_meetings_by_room_id(db: Session, room_id: UUID):
    return db.query(Meeting).filter(Meeting.room_id == room_id).all()

def create_meeting(db: Session, meeting:Meeting):
    room = room_repository.get_room_by_id(db, meeting.room_id)
    if room is None:
        raise HTTPException(status_code=404, detail="Room not found")
    room_availability = db.query(Meeting).filter(Meeting.room_id == meeting.room_id).filter(Meeting.start < meeting.end).filter(Meeting.end > meeting.start).all()
    if room_availability:
        raise HTTPException(status_code=404, detail="Room is already booked")
    db.add(meeting)
    db.commit()
    db.refresh(meeting)
    return meeting

def delete_meeting_by_id(db: Session, meeting_id: UUID):
    meeting = get_meeting_by_id(db, meeting_id)
    db.delete(meeting)
    db.commit()
    return "Meeting deleted"