from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from typing import Annotated
from sqlalchemy.orm import Session
from fastapi import Depends
from dotenv import load_dotenv
import os

load_dotenv()

db = dict(
    user = os.getenv('DB_USER'),
    password = os.getenv('DB_PASSWORD'),
    host = os.getenv('DB_HOST'),   
    port = os.getenv('DB_PORT'),
    name = os.getenv('DB_NAME'),
    schema = os.getenv('DB_SCHEMA')
)

DATABASE_URL = f'postgresql://{db["user"]}:{db["password"]}@{db["host"]}:5432/{db["name"]}'

engine = create_engine(DATABASE_URL)
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()
    
db_dependency = Annotated[Session, Depends(get_db)]