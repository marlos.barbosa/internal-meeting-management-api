from fastapi import APIRouter
from config.database import db_dependency
from services import meeting as meeting_service
from domain.schemas.meeting import Meeting, MeetingCreate
from uuid import UUID

router = APIRouter()
MEETING_TAG = "Meeting data"

@router.get("/meetings",
           summary="Fetch all meetings",
            tags=[MEETING_TAG],
            response_model=list[Meeting],
            )
def get_meetings(db: db_dependency):
    return meeting_service.get_meetings(db)

@router.get("/meetings/{meeting_id}",
            summary="Fetch a meeting by id",
            tags=[MEETING_TAG],
            response_model=Meeting,
            )
def get_meeting_by_id(meeting_id: UUID, db: db_dependency):
    return meeting_service.get_meeting_by_id(db, meeting_id)

@router.delete("/meetings/{meeting_id}",
               summary="Delete a meeting by id",
               tags=[MEETING_TAG],
               response_model=str,
               )
def delete_meeting_by_id(meeting_id: UUID, db: db_dependency):
    return meeting_service.delete_meeting_by_id(db, meeting_id)

@router.post("/meetings",
             summary="Create a new meeting",
             tags=[MEETING_TAG],
             response_model=Meeting,
             )
def create_meeting(meeting: MeetingCreate, db: db_dependency):
    return meeting_service.create_meeting(db, meeting)