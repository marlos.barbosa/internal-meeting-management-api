from fastapi import APIRouter
from config.database import db_dependency
from uuid import UUID
from services import meeting_employee as meeting_employee_service

router = APIRouter()
MEETING_EMPLOYEE_TAG = "Invite data"

@router.post("/meetings/{meeting_id}/employees/{employee_id}",
             summary="Add an employee to a meeting",
             tags=[MEETING_EMPLOYEE_TAG],
             response_model=str,
)
def add_employee_to_meeting(meeting_id: UUID, employee_id: UUID, db: db_dependency):
    return meeting_employee_service.add_employee_to_meeting(db, meeting_id, employee_id)

@router.delete("/meetings/{meeting_id}/employees/{employee_id}",
               summary="Remove an employee from a meeting",
                tags=[MEETING_EMPLOYEE_TAG],
                response_model=str,
)
def remove_employee_from_meeting(meeting_id: UUID, employee_id: UUID, db: db_dependency):
    return meeting_employee_service.remove_employee_from_meeting(db, meeting_id, employee_id)