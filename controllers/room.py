from fastapi import APIRouter
from config.database import db_dependency
from services import room as room_service
from domain.schemas.room import Room, RoomCreate
from uuid import UUID

router = APIRouter()

ROOM_TAG = "Room data"

@router.get("/rooms",
           summary="Fetch all rooms",
            tags=[ROOM_TAG],
            response_model=list[Room],
            )
def get_rooms(db: db_dependency):
    return room_service.get_rooms(db)

@router.get("/rooms/{room_id}",
            summary="Fetch a room by id",
            tags=[ROOM_TAG],
            response_model=Room,
            )
def get_room_by_id(room_id: UUID, db: db_dependency):
    return room_service.get_room_by_id(db, room_id)

@router.delete("/rooms/{room_id}",
               summary="Delete a room by id",
               tags=[ROOM_TAG],
               response_model=str,
               )
def delete_room_by_id(room_id: UUID, db: db_dependency):
    return room_service.delete_room_by_id(db, room_id)

@router.post("/rooms",
             summary="Create a new room",
             tags=[ROOM_TAG],
             response_model=Room,
             )
def create_room(room: RoomCreate, db: db_dependency):
    return room_service.create_room(db, room)

@router.put("/rooms/{room_id}",
            summary="Update a room by id",
            tags=[ROOM_TAG],
            response_model=Room,
            )
def update_room(room_id: UUID, new_name: str, db: db_dependency):
    return room_service.update_room(db, room_id, new_name)


