from sqlalchemy.orm import Session
from fastapi import HTTPException
from repositories import employee as employee_repository
from domain.schemas.employee import EmployeeCreate
from domain.models.employee import Employee
from repositories import meeting_employee as meeting_employee_repository
from uuid import UUID

EMPLOYEE_NOT_FOUND = "Employee not found"

def get_employees(db: Session):
    return employee_repository.get_employees(db)

def get_employee_by_id(db: Session, employee_id: UUID):
    employee = employee_repository.get_employee_by_id(db, employee_id)
    if employee is None:
        raise HTTPException(status_code=404, detail=EMPLOYEE_NOT_FOUND)
    return employee_repository.get_employee_by_id(db, employee_id)

def create_employee(db: Session, employee:EmployeeCreate):
    checking_employee = employee_repository.get_employee_by_email(db, employee.email)
    if checking_employee:
        raise HTTPException(status_code=404, detail="Email already used")
    employee = Employee(**employee.dict())
    return employee_repository.create_employee(db, employee)

def delete_employee_by_id(db: Session, employee_id: UUID):
    employee = employee_repository.get_employee_by_id(db, employee_id)
    if employee is None:
        raise HTTPException(status_code=404, detail=EMPLOYEE_NOT_FOUND)
    employee_meeting = meeting_employee_repository.get_meeting_employee_by_employee_id(db, employee_id) 
    if employee_meeting:
        raise HTTPException(status_code=404, detail="Employee has associated meetings. Cancel meetings first or remove employee from meetings first.")
    return employee_repository.delete_employee_by_id(db, employee_id)

def update_employee(db: Session, employee_id: UUID, employee:EmployeeCreate):
    employee_checking = employee_repository.get_employee_by_id(db, employee_id)
    if employee_checking is None:
        raise HTTPException(status_code=404, detail=EMPLOYEE_NOT_FOUND)
    return employee_repository.update_employee(db, employee_id, employee)