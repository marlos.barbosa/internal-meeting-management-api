from sqlalchemy.orm import Session
from repositories import meeting_employee as meeting_employee_repository
from repositories import employee as employee_repository
from repositories import meeting as meeting_repository
from uuid import UUID
from fastapi import HTTPException

def add_employee_to_meeting(db: Session, meeting_id: UUID, employee_id: UUID):
    employee = employee_repository.get_employee_by_id(db, employee_id)
    if employee is None:
        raise HTTPException(status_code=404, detail="Employee not found")
    meeting = meeting_repository.get_meeting_by_id(db, meeting_id)
    if meeting is None:
        raise HTTPException(status_code=404, detail="Meeting not found")
    employee_time_availability = meeting_employee_repository.get_employee_availability(db, employee_id, meeting_id)
    if employee_time_availability:
        raise HTTPException(status_code=404, detail="Employee is not available at this time")
    return meeting_employee_repository.add_employee_to_meeting(db, meeting_id, employee_id)

def remove_employee_from_meeting(db: Session, meeting_id: UUID, employee_id: UUID):
    employee = employee_repository.get_employee_by_id(db, employee_id)
    if employee is None:
        raise HTTPException(status_code=404, detail="Employee not found")
    meeting = meeting_repository.get_meeting_by_id(db, meeting_id)
    if meeting is None:
        raise HTTPException(status_code=404, detail="Meeting not found")
    meeting_employee = meeting_employee_repository.get_meeting_employee(db, meeting_id, employee_id)
    if meeting_employee is None:
        raise HTTPException(status_code=404, detail="Employee is not in the meeting")
    return meeting_employee_repository.remove_employee_from_meeting(db, meeting_id, employee_id)