from sqlalchemy.orm import Session
from repositories import room as room_repository
from repositories import meeting as meeting_repository
from domain.schemas.room import RoomCreate
from domain.models.room import Room
from fastapi import HTTPException
from uuid import UUID

ROOM_NOT_FOUND = "Room not found"

def get_rooms(db: Session):
    return room_repository.get_rooms(db)

def get_room_by_id(db: Session, room_id: UUID):
    room = room_repository.get_room_by_id(db, room_id)
    if room is None:
        raise HTTPException(status_code=404, detail=ROOM_NOT_FOUND)
    return room_repository.get_room_by_id(db, room_id)

def create_room(db: Session, room:RoomCreate):
    new_room = Room(**room.dict())
    room_checking = room_repository.get_room_by_name(db, new_room.name)
    if room_checking:
        raise HTTPException(status_code=404, detail="Room name already used")
    return room_repository.create_room(db, new_room)

def delete_room_by_id(db: Session, room_id: UUID):
    room = room_repository.get_room_by_id(db, room_id)
    if room is None:
        raise HTTPException(status_code=404, detail=ROOM_NOT_FOUND)
    associated_meetings = meeting_repository.get_meetings_by_room_id(db, room_id)
    if associated_meetings:
        raise HTTPException(status_code=404, detail="Room has associated meetings. Cancel meetings first.")
    return room_repository.delete_room_by_id(db, room_id)

def update_room(db: Session, room_id: UUID, new_name: str):
    room = room_repository.get_room_by_id(db, room_id)
    if room is None:
        raise HTTPException(status_code=404, detail=ROOM_NOT_FOUND)
    return room_repository.update_room(db, room_id, new_name)