INSERT INTO internal_meeting_management.rooms VALUES ('3ec63868-3d41-4476-9291-cd96298e7d36','Sala de reuniões 01');
INSERT INTO internal_meeting_management.rooms VALUES ('d5acbc96-cb85-4288-b82a-f683ed06d278','Sala de reuniões 02');

INSERT INTO internal_meeting_management.employees VALUES ('55737620-20de-415c-9cc7-f80788c3fc4b','João Carlos','joao.carlos@petrobreizius.com');
INSERT INTO internal_meeting_management.employees VALUES ('1114cf7f-4e46-4b62-b5c1-370c3c0591ab','João Guilherme','joao.guilherme@petrobreizius.com');
INSERT INTO internal_meeting_management.employees VALUES ('c9d5d83c-c260-453c-aa33-32b175e74969','João Vitor','joao.vitor@petrobreizius.com');

INSERT INTO internal_meeting_management.meetings VALUES ('f2e28f8b-ea72-4a8a-be42-db99d061fd5a','Reunião de planejamento','2024-04-03 09:00:00','2024-04-03 10:00:00','3ec63868-3d41-4476-9291-cd96298e7d36');
INSERT INTO internal_meeting_management.meetings VALUES ('cde0acd0-fe3e-45a0-9ec5-a63a9c0a790a','Reunião de retrospectiva','2024-04-03 10:00:00','2024-04-03 11:00:00','3ec63868-3d41-4476-9291-cd96298e7d36');
INSERT INTO internal_meeting_management.meetings VALUES ('cc437c57-44f0-4b95-981f-09ddf13dd4ed','Reunião de feedback','2024-04-03 10:00:00','2024-04-03 11:00:00','d5acbc96-cb85-4288-b82a-f683ed06d278');

INSERT INTO internal_meeting_management.meeting_employee VALUES ('c5120b92-499a-4b26-a57a-ea3d8336fb63','f2e28f8b-ea72-4a8a-be42-db99d061fd5a','55737620-20de-415c-9cc7-f80788c3fc4b');
INSERT INTO internal_meeting_management.meeting_employee VALUES ('e9c3c785-b9b6-4ff2-9f9c-def9cbf22f20','f2e28f8b-ea72-4a8a-be42-db99d061fd5a','1114cf7f-4e46-4b62-b5c1-370c3c0591ab');
INSERT INTO internal_meeting_management.meeting_employee VALUES ('7a529c8c-8008-4330-b6f9-f57edb8b8ed2','f2e28f8b-ea72-4a8a-be42-db99d061fd5a','c9d5d83c-c260-453c-aa33-32b175e74969');

INSERT INTO internal_meeting_management.meeting_employee VALUES ('ec6bf27d-e7a3-4781-8cd7-3c2ab927e198','cde0acd0-fe3e-45a0-9ec5-a63a9c0a790a','55737620-20de-415c-9cc7-f80788c3fc4b');
INSERT INTO internal_meeting_management.meeting_employee VALUES ('01024fe7-fa02-4d33-8e66-1c1b05ad0b7b','cde0acd0-fe3e-45a0-9ec5-a63a9c0a790a','1114cf7f-4e46-4b62-b5c1-370c3c0591ab');
INSERT INTO internal_meeting_management.meeting_employee VALUES ('428f2e8a-972a-4776-8931-8e152c9ee7bb','cde0acd0-fe3e-45a0-9ec5-a63a9c0a790a','c9d5d83c-c260-453c-aa33-32b175e74969');

INSERT INTO internal_meeting_management.meeting_employee VALUES ('d6cd8477-1c32-4efe-ae25-55c5363063a0','cc437c57-44f0-4b95-981f-09ddf13dd4ed','55737620-20de-415c-9cc7-f80788c3fc4b');
INSERT INTO internal_meeting_management.meeting_employee VALUES ('591eeb6a-fb07-4021-aaa2-05b25ccba451','cc437c57-44f0-4b95-981f-09ddf13dd4ed','1114cf7f-4e46-4b62-b5c1-370c3c0591ab');