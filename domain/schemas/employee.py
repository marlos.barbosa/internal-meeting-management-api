from domain.schemas.generic import GenericModel
from pydantic import Field, EmailStr
from uuid import UUID

class EmployeeBase(GenericModel):
    name: str = Field(default="Carlos Alberto")
    email: EmailStr = Field(default="carlos.alberto@petrobreizius.com")

class EmployeeCreate(EmployeeBase):
    pass

class Employee(EmployeeBase):
    id: UUID = Field(default='9d47cb70-9195-4aad-86c8-3211cdaea0ec')

    class Config:
        from_attributes = True