from datetime import datetime, timedelta
from domain.schemas.generic import GenericModel
from domain.schemas.employee import Employee 
from domain.schemas.room import Room
from pydantic import Field
from uuid import UUID

class MeetingBase(GenericModel):
    subject:str = Field(default="Reunião de alinhamento")
    start:datetime = Field(default=datetime.now())
    end:datetime = Field(default=datetime.now() + timedelta(hours=1))

class MeetingCreate(MeetingBase):
    room_id:UUID =  Field(default='692ee7bb-c13f-42fd-8371-d2daa761f6cc')

class Meeting(MeetingBase):
    id: UUID = Field(default='19462987-c2e8-4ee8-97c4-8ecd2ad8d6a1')
    room:Room
    participants:list[Employee] = []

    class Config:
        from_attributes = True