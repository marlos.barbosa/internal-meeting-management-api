from domain.schemas.generic import GenericModel
from pydantic import Field
from uuid import UUID

class RoomBase(GenericModel):
    name: str = Field(default="Sala de reuniões")

class RoomCreate(RoomBase):
    pass

class Room(RoomBase):
    id: UUID = Field(default='2f4845c7-d19f-4100-81b3-de9facf81852')

    class Config:
        from_attributes = True