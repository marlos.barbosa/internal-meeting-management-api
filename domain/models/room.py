from domain.models.generic import GenericBase
import uuid
from sqlalchemy.dialects.postgresql import UUID

from sqlalchemy.orm import relationship
from sqlalchemy import Column, String


class Room(GenericBase):
    __tablename__ = 'rooms'

    id:UUID = Column("room_cd_room", UUID, primary_key=True, default=uuid.uuid4)
    name:str = Column("room_nm_room", String, unique=True)

    meetings = relationship("Meeting", back_populates="room")