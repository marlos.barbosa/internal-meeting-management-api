from domain.models.generic import GenericBase
import uuid
from sqlalchemy.dialects.postgresql import UUID

from sqlalchemy.orm import relationship
from sqlalchemy import Column, String

from config.database import db



class Employee(GenericBase):
    __tablename__ = 'employees'

    id:UUID = Column("empl_cd_employee", UUID, primary_key=True, default=uuid.uuid4)
    name:str = Column("empl_nm_employee", String)
    email:str = Column("empl_tx_email", String, unique=True)

    meetings = relationship("Meeting", secondary=f'{db["schema"]}.meeting_employee' , back_populates="participants")