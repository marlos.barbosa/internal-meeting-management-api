from domain.models.generic import GenericBase
import uuid
from sqlalchemy.dialects.postgresql import UUID


from sqlalchemy import Column, ForeignKey, UniqueConstraint
from config.database import db


class MeetingEmployee(GenericBase):
    __tablename__ = 'meeting_employee'

    __table_args__ = (
        UniqueConstraint('meet_cd_meeting', 'empl_cd_employee'),
        )

    id:UUID = Column("meem_cd_meeting_employee", UUID, primary_key=True, default=uuid.uuid4)

    meeting_id = Column("meet_cd_meeting", UUID, ForeignKey(f'{db["schema"]}.meetings.meet_cd_meeting'))
    employee_id = Column("empl_cd_employee", UUID, ForeignKey(f'{db["schema"]}.employees.empl_cd_employee'))