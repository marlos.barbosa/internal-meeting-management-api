from domain.models.generic import GenericBase
import uuid
from sqlalchemy.dialects.postgresql import UUID

from sqlalchemy.orm import relationship
from sqlalchemy import Column, String, ForeignKey, DateTime

from config.database import db


class Meeting(GenericBase):
    __tablename__ = 'meetings'

    id:UUID = Column("meet_cd_meeting", UUID, primary_key=True, default=uuid.uuid4)
    subject:str = Column("meet_nm_subject", String)
    start:DateTime = Column("meet_dt_start", DateTime)
    end:DateTime = Column("meet_dt_end", DateTime)

    room_id = Column("room_cd_room", UUID, ForeignKey('rooms.room_cd_room'))
    room = relationship("Room", back_populates="meetings")

    participants = relationship("Employee", secondary=f'{db["schema"]}.meeting_employee' , back_populates="meetings")