from sqlalchemy import MetaData
from sqlalchemy.ext.declarative import declarative_base

metadata=MetaData(schema="internal_meeting_management")
Base = declarative_base(metadata=metadata)

class GenericBase(Base):
    __abstract__ = True

